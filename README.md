# build-uboot

Build uboot tarball with the latest version of uboot for raspberry pi's, and upload to the package registry.  Checks to see if it is already built first.

| CI/Pipeline variables | Description |
| -------- | -------------------------------------------------- |
| BUILDVER | version to build eg 2021.10 , if not already built. If unset latest version is used |
| BUILDIT  | set to force build even if built already           |
